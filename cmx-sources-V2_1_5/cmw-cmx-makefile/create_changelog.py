#!/usr/bin/python
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# coding: utf-8
"""create changelog from svn"""
from lxml.etree import parse
from subprocess import Popen, PIPE
from urllib2 import urlopen
import json
import itertools
import re
import argparse

JIRA_REST_URL_ISSUE = "https://issues.cern.ch/rest/api/2/issue/"

def get_svn_ls(url):
    """get list of directory entries"""
    proc = Popen(["svn", "ls", "--xml", url], stdout=PIPE)
    return sorted(map(lambda elem: (elem.xpath("commit/@revision")[0], 
                             elem.xpath("name/text()")[0]),
                      parse(proc.stdout).xpath('/lists/list/entry')))

def get_svn_revision(url):
    """get latest svn revision number of this path"""
    proc = Popen(["svn", "log", "--xml", "-l", "1", url], stdout=PIPE)
    return parse(proc.stdout).xpath('/log/logentry/@revision')[0]

def get_log_entries(url, rev1, rev2):
    """get logmessages from rev1..rev2"""
    proc = Popen(["svn", "log", "--xml", "-r", "{0}:{1}".format(rev1, rev2), 
                  url], 
              stdout=PIPE)
    for entry in parse(proc.stdout).xpath('/log/logentry'):
        yield (entry.xpath('author/text()')[0], entry.xpath('msg/text()')[0])

def get_jira_issue(issue_key):
    """load info about jira issue using http-rest api"""
    body = urlopen(JIRA_REST_URL_ISSUE + issue_key)
    data = json.load(body)
    return data

def print_message(entries, msg_format):
    """print jira issues"""
    issues = {}
    for author, msg in entries:
        match = re.match("[^A-Z]*([A-Z]{1,5}-[0-9]{1,5})", msg)
        if match:
            issue_key = match.groups()[0]
            if issue_key in issues:
                issues[issue_key].add(author)
            else:
                issues[issue_key] = set([author])

    work = issues.items()
    work.sort(key=lambda x: list(x[1]))
    for authors, data in itertools.groupby(work, lambda x:x[1]):
        print "** Committers:", ", ".join(authors)
        print ""
        for issue_key, _ in data:
            issue = get_jira_issue(issue_key)
            print msg_format.format(**issue)

DEFAULT_FORMAT ="*** *{key}* {fields[summary]}" + \
        " (Reporter: {fields[reporter][name]}," + \
        " Assignee: {fields[assignee][name]}, Status: {fields[status][name]})"

def main():
    """main"""
    parser = argparse.ArgumentParser(description="Creates Changelog from" + \
                                     " commit messages starting from" + \
                                     " 'start_url' until including 'end_url'")
    parser.add_argument("--format", 
        help="Format of message from jira response. Default:" + DEFAULT_FORMAT,
        default=DEFAULT_FORMAT)
   
    parser.add_argument("start_url", help="e.g. previous SVN Tag or directory of svn tags")
    parser.add_argument("end_url", nargs="?", help="e.g. current trunk")
    args = parser.parse_args()
    
    if args.end_url:
        rev1 = get_svn_revision(args.start_url)
        rev2 = get_svn_revision(args.end_url)
        
        print_message(get_log_entries(args.end_url, rev1, rev2), args.format)
    else:
        print "== Changelog {0}".format(args.start_url)
        print ""
        versions = get_svn_ls(args.start_url)
        for version1, version2 in reversed(zip(versions,versions[1:])):
            print "* Version {0}".format(version2[1])
            print_message(get_log_entries(args.start_url + "/" + version2[1], version1[0], version2[0]), args.format)
            print ""



if __name__ == '__main__':
    main()