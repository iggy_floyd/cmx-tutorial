/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <ctime>
#include <iostream>
#include <cmw-cmx-cpp/ProcessComponent.h>
#include <cmw-cmx-cpp/Component.h>

using namespace cmw::cmx;

int main()
{
    struct timespec tm;
    tm.tv_sec = 0;
    tm.tv_nsec = 50000000;

    ProcessComponent::update();

    ComponentPtr component = Component::create("stats");
    CmxInt64 metr_test = component->newInt64("test");

    std::cout << "Enter work-sleep loop" << std::endl;
    for (int i = 0; i < 100; i++)
    {
        metr_test = i;  // update metric

        if (i % 500 == 0) ProcessComponent::update(); // update process metrics
        (std::cout << ".").flush();
        nanosleep(&tm, NULL);
    }
}
