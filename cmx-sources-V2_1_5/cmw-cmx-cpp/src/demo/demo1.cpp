/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <iostream>
extern "C"
{
#include <unistd.h>
}
#include <cmw-cmx-cpp/ProcessComponent.h>
#include <cmw-cmx-cpp/demo/Demo1.h>

using namespace cmw::cmx;
using namespace cmw::cmx::demo;

int main(int argc, char * argv[])
{
    ProcessComponent::update();
    std::cout << "Running demo" << std::endl;

    Demo1 c;
    while (1)
    {
        c.execute();
        usleep(100000);
    }
}
