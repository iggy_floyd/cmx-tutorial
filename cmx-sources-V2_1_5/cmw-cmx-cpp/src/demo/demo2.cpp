/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <iostream>
extern "C"
{
#include <unistd.h>
}
#include <cmw-cmx-cpp/demo/Demo2.h>

using namespace cmw::cmx;
using namespace cmw::cmx::demo;

int main(int argc, char * argv[])
{
    std::cout << "Running demo" << std::endl;
    Demo2::updateProcess();

    Demo2 c1("c1");
    Demo2 c2("c2");
    while (1)
    {
        c1.execute();
        c2.execute();
        usleep(100000);
    }
}
