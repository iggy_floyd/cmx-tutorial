/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/ProcessComponent.h>

#include <iostream>

extern "C"
{
#include <cmw-cmx/common.h>
}

using namespace cmw::cmx;

int main()
{
    ProcessComponent::update();
    uint64_t time = cmx_common_current_time_usec();
    uint32_t counter = 0;
    ComponentPtr component = Component::create("measure_fail-cpp");
    CmxInt64 value = component->newInt64("Test");

    while (1)
    {
        if (cmx_common_current_time_usec() - time > 1000000)
        {
            std::cout << counter << " = " << (counter / 1000) << "K updates/s" << std::endl;
            counter = 0;
            time = cmx_common_current_time_usec();
        }
        value = (int64_t) counter;
        counter++;
    }

    return 0;
}
