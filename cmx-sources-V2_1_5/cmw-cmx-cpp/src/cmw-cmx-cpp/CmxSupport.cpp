/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cmw-cmx-cpp/Registry.h>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/CmxSupport.h>
#include <cmw-cmx-cpp/impl/CmxSupportImpl.h>

namespace cmw
{
namespace cmx
{

CmxSupport::CmxSupport(bool ignoreError) :
                impl(new impl::CmxSupportImpl(ignoreError))
{

}

CmxSupport::~CmxSupport()
{
    delete impl;
}

CmxInt64 CmxSupport::newInt64(const std::string & component_name, const std::string & value_name)
{
    return impl->newInt64(component_name, value_name);
}

CmxFloat64 CmxSupport::newFloat64(const std::string & component_name, const std::string & value_name)
{
    return impl->newFloat64(component_name, value_name);
}

CmxBool CmxSupport::newBool(const std::string & component_name, const std::string & value_name)
{
    return impl->newBool(component_name, value_name);
}

CmxString CmxSupport::newString(const std::string & component_name, const std::string & value_name, const uint16_t size)
{
    return impl->newString(component_name, value_name, size);
}

} // namespace cmx
} // namespace cmw
