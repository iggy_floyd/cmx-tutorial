/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_SUPPORT
#define HAVE_CMW_CMX_SUPPORT
#include <cmw-cmx-cpp/CmxTypes.h>

namespace cmw
{
namespace cmx
{

// \see impl::CmxSupportImpl
namespace impl
{
class CmxSupportImpl;
}

class CmxSupport
{
public:
    CmxSupport(bool ignoreError = true);
    ~CmxSupport();
protected:
    CmxInt64 newInt64(const std::string & component_name, const std::string & value_name);
    CmxFloat64 newFloat64(const std::string & component_name, const std::string & value_name);
    CmxBool newBool(const std::string & component_name, const std::string & value_name);
    CmxString newString(const std::string & component_name, const std::string  & value_name, const uint16_t size);
private:
    impl::CmxSupportImpl * impl;
};

} // namespace cmx
} // namespace cmw
#endif
