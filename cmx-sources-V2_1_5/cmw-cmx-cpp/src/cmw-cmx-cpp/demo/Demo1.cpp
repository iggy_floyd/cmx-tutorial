/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <sstream>

#include <cmw-cmx-cpp/demo/Demo1.h>

namespace cmw
{
namespace cmx
{
namespace demo
{

Demo1::Demo1() :
                counterInt(newInt64("Component1", "metric_int")),
                counterFloat(newFloat64("Component1", "metric_float")),
                counterBool(newBool("Component1", "metric_bool")),
                counterString(newString("Component1", "metric_string", 30))
{
}

void Demo1::execute()
{
    counterInt = (counterInt + 1);
    counterFloat = (counterFloat + 0.123f);
    counterBool = !counterBool;

    std::ostringstream os;
    os << "toString(counterInt)=" << counterInt;
    counterString = os.str();
}
} // namespace demo
} // namespace cmx
} // namespace cmw
