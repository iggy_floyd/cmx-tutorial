/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_LOG
#define HAVE_CMW_CMX_LOG

namespace cmw
{
namespace cmx
{
namespace Log
{
#define LOG_POSITION() (cmw::cmx::Log::Position(__FILE__, __LINE__, __FUNCTION__))
struct Position
{
    Position(const char * source, const int line, const char * function) :
                    source_(source),
                    line_(line),
                    function_(function)
    {
    }
    const char * source_;
    const int line_;
    const char * function_;
};

/**
 * \brief Expose the enum log_levels values from cmx-c
 */
struct level
{
    static const int AUDIT ;
    static const int ERROR;
    static const int WARN;
    static const int INFO;
    static const int DEBUG;
    static const int TRACE;
};

/*
 * \see setLogAdapter()
 */
typedef void (*log_adapter_t)(int log_level, const char * message, bool truncated);

/**
 * \brief set a function to use to redirect the cmx logging.
 *
 * Use NULL as adapter_func to reset.
 *
 * Messages will be truncated to 256 bytes.
 */
void setLogAdapter(log_adapter_t adapter_func);

/**
 * \brief Set the log level.
 *
 * Online effective if not using a log adapter.
 * Set to -1 to let the c implementation re-intialize the log level from environment or default.
 * \see level
 */
void setLogLevel(int level);

/**
 * \see setLogLevel
 */
int getLogLevel();

void audit(const Position & p, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));
void error(const Position & p, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));
void warn(const Position & p, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));
void info(const Position & p, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));
void debug(const Position & p, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));
void trace(const Position & p, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));

} // namespace Log
} // namespace cmx
} // namespace cmw
#endif
