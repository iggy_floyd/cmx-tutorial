/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_SUPPORTIMPL
#define HAVE_CMW_CMX_SUPPORTIMPL
#include <boost/shared_ptr.hpp>
#include <map>
#include <cmw-cmx-cpp/CmxTypes.h>
#include <cmw-cmx-cpp/Component.h>
#include <pthread.h>

namespace cmw
{
namespace cmx
{
namespace impl
{

using namespace cmw::cmx;

class CmxSupportImpl
{
    pthread_mutex_t componentsLock;
    std::map<std::string, ComponentPtr> components;
public:
    CmxSupportImpl(bool ignoreError = true);
    CmxInt64 newInt64(const std::string & component_name, const std::string & value_name);
    CmxFloat64 newFloat64(const std::string & component_name, const std::string & value_name);
    CmxBool newBool(const std::string & component_name, const std::string & value_name);
    CmxString newString(const std::string & component_name, const std::string & value_name, const uint16_t size);
private:
    bool ignoreError_;
    ComponentPtr getComponent(const std::string & name);
};

} // namespace impl
} // namespace cmx
} // namespace cmw
#endif
