/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_COMPONENT
#define HAVE_CMW_CMX_COMPONENT
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <map>

#include <cmw-cmx-cpp/ImmutableComponent.h>
#include <cmw-cmx-cpp/CmxTypes.h>
#include <cmw-cmx-cpp/Log.h>

extern "C"
{
#include <stdint.h>
}

namespace cmw
{
namespace cmx
{

class Component;
typedef boost::shared_ptr<Component> ComponentPtr;

/**
 * \brief CMX Component
 *
 * The Component is the mutable version ImmutableComponent and inherits all properties.
 *
 * Additional you can add/set/remove Values. Also new Components are created using the static ::create Method.
 *
 * \note Adding new Values: Please take attention in this step. Components are garbage collected through reference counting
 *                    (shared_ptr) or process existence checking (cleanup dead components in case of unclean exits).
 *                    On the contrary with every value you add (and never remove) the operation system will need to
 *                    actual wire more memory to your process. If you keep adding metrics without remove this will
 *                    render the values less value for diagnostic tools, CMX also does not check for duplicate value
 *                    names so your data becomes ambigious.
 */
class Component : public ImmutableComponent
{
public:
    /// Create a new Integer value
    CmxInt64 newInt64(const std::string & name);
    /// Create a new Float value
    CmxFloat64 newFloat64(const std::string & name);
    /// Create a new Boolean value
    CmxBool newBool(const std::string & name);
    /**
     * \brief Creates a new String value
     * \param name      Name of the new value
     * \param size      max. number of characters in this value
     */
    CmxString newString(const std::string & name, const uint16_t size);

    /// Update a Integer value
    void set(const CmxInt64 & ref, const CmxInt64::c_type & value);
    /// Update a Float value
    void set(const CmxFloat64 & ref, const CmxFloat64::c_type & value);
    /// Update a Boolean value
    void set(const CmxBool & ref, const CmxBool::c_type & value);
    /// Update a String value
    void set(const CmxString & ref, const CmxString::c_type & value);

    /**
     * \brief remove a value
     *
     * This makes a CmxRef instances referring to the same value invalid.
     */
    void remove(const CmxRef & ref);

    /**
     * \brief Create a new component with current process id and specified name.
     * \param name         Name of the new component
     * \param ignoreError
     *                     - Is passed to component.
     *                     - If true this component will not throw exception
     *                       but ignore errors silently (the c library will still log errors).
     *                     - If the component creation fails and ignoreError = true then this function
     *                       will return a dummy component.
     */
    static ComponentPtr create(const std::string name, bool ignoreError = false);

    /**
     * \brief Create a dummy object that does nothing.
     *
     * All calls will be silently ignored. getValue() and implicit get() on
     * Cmx[Int,Float..] Objects will return a neutral value (0,0.0,false,"").
     */
    static ComponentPtr dummy();

    /**
     * \brief Remove the CMX Component. Also destroy the associated shared memory object.
     * \see ImmutableComponent::~ImmutableComponent()
     */
    ~Component();
private:
    boost::weak_ptr<Component> weakSelf;
    const bool dummy_;
    Component(const Component& that); // = delete

    /// create a dummy component
    Component();
    /// create a cmx-backed component
    Component(void * cmx_shm_ptr);
};

} // namespace cmx
} // namespace cmw
#endif
