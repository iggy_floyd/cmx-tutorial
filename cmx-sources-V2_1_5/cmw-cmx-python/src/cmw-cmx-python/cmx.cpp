#include "cmx.h"

list Registry_list()
{
    list l;
    for (Registry::iterator it = Registry::begin(); it != Registry::end(); ++it)
    {
        l.append(*it);
    }
    return l;
}

list ImmutableComponent_list(ImmutableComponent * self)
{
    list l;
    for (ImmutableComponent::iterator it = self->begin(); it != self->end(); ++it)
    {
        CmxRef x(*it);
        if (x.type() == CmxTypeTagInt64::cmx_type) l.append(cmx_cast<CmxTypeTagInt64>(x));
        else if (x.type() == CmxTypeTagFloat64::cmx_type) l.append(cmx_cast<CmxTypeTagFloat64>(x));
        else if (x.type() == CmxTypeTagBool::cmx_type) l.append(cmx_cast<CmxTypeTagBool>(x));
        else if (x.type() == CmxTypeTagString::cmx_type) l.append(cmx_cast<CmxTypeTagString>(x));
    }

    return l;
}
