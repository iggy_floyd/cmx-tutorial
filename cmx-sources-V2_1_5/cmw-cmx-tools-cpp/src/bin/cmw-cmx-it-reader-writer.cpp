/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/*
 * This is a integration Test. It is splitted in two parts: A reader, a writer.
 * The this is successful if both parts a exit with returncode 0.
 * The split in two parts enable to run mixed 32/64 bit.
 */
extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <sched.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

// CMX
#include <cmw-cmx/shm.h>
#include <cmw-cmx/registry.h>
#include <cmw-cmx/process.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/atomic.h>
}

static const char * const COMPONENT_NAME = "it-reader-writer";
static const char * const VALUE_NAME = "testint";

int run = 1;
unsigned int seed;

static void sighandler_usr1(int signame, siginfo_t * siginfo, void * arg)
{
    cmx_atomic_set_int32(&run, 0);
    cmx_atomic_smp_mb();
}

static void set_sighandler()
{
    struct sigaction sigaction_info;
    sigset_t block_mask;
    sigemptyset(&block_mask);

    sigaction_info.sa_sigaction = sighandler_usr1;
    sigaction_info.sa_flags = 0;
    sigaction_info.sa_flags |= SA_SIGINFO;
    sigaction_info.sa_flags |= SA_NODEFER; // not sure why this is needed
    sigaction_info.sa_flags |= SA_RESETHAND; // automatically reset handle after one signal is received
    sigaction_info.sa_mask = block_mask;

    if (sigaction(SIGUSR1, &sigaction_info, NULL) != 0)
    {
        fprintf(stderr, "failed to set usr1 sighandler\n");
        exit(1);
    }
}
int writer()
{
    int64_t no_of_writes = 0;
    cmx_shm * cmx_shm_ptr;
    cmx_shm_value shm_value;
    int value_handle;

    if (cmx_shm_create(COMPONENT_NAME, &cmx_shm_ptr) != E_CMX_SUCCESS)
    {
        fprintf(stderr, "Failed to create cmx component\n");
        return 1;
    }

    value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, VALUE_NAME);
    if (value_handle < 0)
    {
        fprintf(stderr, "Failed to create cmx value\n");
        return 2;
    }

    while (cmx_atomic_read_int32(&run))
    {
        // mutate value
        int random_value = ::rand_r(&seed);
        shm_value._int64 = (shm_value._int64 ^ random_value) && 0xfffffffe; // mutate value
        if (shm_value._int64 % 2 == 0) // if % 2 ==0 then set lowest bit
        {
            // set "checksum" bit
            shm_value._int64 |= 0xf;
        }

        // set
        if (cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_INT64, &shm_value) == E_CMX_SUCCESS)
        {
            no_of_writes++;
        }
        else
        {
            fprintf(stderr, "Set value failed\n");
            return 3;
        }
        sched_yield();
    }

    if (no_of_writes == 0)
    {
        fprintf(stderr, "no_of_writes =0\n");
        return 4;
    }

    printf("no_of_writes=%" PRIdFAST64 "\n", no_of_writes);

    return 0;
}

int reader()
{
    cmx_shm * cmx_shm_ptr = NULL;
    int value_handle;
    int64_t no_of_reads = 0;
    int64_t no_of_reads_fail = 0;
    {
        cmx_component_info * component_info;
        int component_len;
        int type;

        if (cmx_registry_cleanup() != E_CMX_SUCCESS)
        {
            fprintf(stderr, "cmx_registry_cleanup failed\n");
            return 1;
        }
        if (cmx_registry_discover(&component_info, &component_len) != E_CMX_SUCCESS)
        {
            fprintf(stderr, "cmx_registry_discover failed\n");
            return 2;
        }
        for (int i = 0; i < component_len; i++)
        {
            if (strncmp(component_info[i].name, COMPONENT_NAME, CMX_NAME_SZ) == 0)
            {
                if (cmx_shm_open_ro(component_info[i].process_id, component_info[i].name, &cmx_shm_ptr)
                        != E_CMX_SUCCESS)
                {
                    fprintf(stderr, "cmx_shm_open_ro failed");
                    free(component_info);
                    return 3;
                }
                break;
            }
        }
        free(component_info);

        if (cmx_shm_ptr == NULL)
        {
            fprintf(stderr, "No component named '%s' found\n", COMPONENT_NAME);
            return 4;
        }

        if (cmx_shm_find_value(cmx_shm_ptr, 0, &value_handle, &type) != E_CMX_SUCCESS)
        {
            fprintf(stderr, "Found no value in cmx compnent\n");
            return 5;
        }
        if (type != CMX_TYPE_INT64)
        {

            fprintf(stderr, "invalid type\n");
            return 6;
        }
    }

    {
        cmx_shm_value shm_value;
        int64_t value;

        while (cmx_atomic_read_int32(&run))
        {
            int r = cmx_shm_get_value_single(cmx_shm_ptr, value_handle, NULL, &shm_value, NULL);
            if (r == E_CMX_CONCURRENT_MODIFICATION)
            {
                no_of_reads_fail++;
                continue;
            }
            if (r != E_CMX_SUCCESS)
            {
                fprintf(stderr, "read failed: %s\n", cmx_common_strerror(r));
                return 7;
            }

            no_of_reads++;
            value = shm_value._int64;

            // test the "checksum"
            if ((value & 0xfffffffe) % 2 == 0 && (value & 0x1))
            {
                continue;
            }
            else if (((value & 0xfffffffe) % 2 == 0 && (value & 0x1) == 0))
            {
                continue;
            }
            else
            {
                fprintf(stderr, "Read invalid value: %" PRIxFAST64 "\n", (uint64_t) value);
                return 8;
            }
        }
    }
    printf("no_of_reads_succ=%" PRIdFAST64 "\n", no_of_reads);
    printf("no_of_reads_fail=%" PRIdFAST64 "\n", no_of_reads_fail);
    return 0;
}

static void help(const char * programname)
{
    printf("%s Invocation:\n", programname);
    printf("\n");
    printf(" -r   Start reader\n");
    printf(" -w   Start writer\n");
    printf("\n");
    printf("Exitcode is non-zero on error. Errors messages will be written to stdout.\n");
}

int main(int argc, char * argv[])
{
    seed = time(NULL) % 12345;

    int opt;
    if (argc == 1)
    {
        help(argv[0]);
        return 0;
    }

    while ((opt = getopt(argc, argv, "rwh")) != -1)
    {
        switch (opt)
        {
            case 'r':
                set_sighandler();
                return reader();
                break;
            case 'w':
                set_sighandler();
                return writer();
                break;
            case 'h':
            default:
                help(argv[0]);
                break;
        }
    }

//    cmx_atomic_set_int32(&run, 0);
//    pthread_create(&reader, NULL, thread_single3_reader, NULL);
//    pthread_create(&writer1, NULL, thread_single3_writer, &writer1_opts);
//    pthread_create(&writer2, NULL, thread_single3_writer, &writer2_opts);
//    cmx_atomic_set_int32(&run, 1);
//    sleep(5);
//    cmx_atomic_set_int32(&run, 0);
//    pthread_join(writer1, NULL);
//    pthread_join(writer2, NULL);
//    pthread_join(reader, NULL);
//
//    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
//    EXPECT_EQ(0, single3_read_invalid) << "strings2_read_invalid > 0 means data-corruption";
//    EXPECT_GT(single3_read_valid, 25000) << "reader made no progress";
//    EXPECT_GT(single3_writes, 250000) << "writer made no progress";

    return 0;
}
