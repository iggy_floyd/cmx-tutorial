/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/*! \mainpage CMX Tools
 * \section ABOUT_CMX About CMX Tools
 * \htmlonly
 * <h2>This is CMX-tools version: $(VERSION)</h2>
 * \endhtmlonly
 *
 * For more information visit CMX on https://cern.ch/cmx and/or https://wikis.cern.ch/display/MW/CMX
 *
 * \section CHANGELOG Changelog
 * The changelog of CMX is a shared document for all CMX products.
 * For the combined Changelog refer to the CMX C API documentation.
 *
 * \section READER cmw-cmx-reader
 * This is the easiest way to see the desired data (in case we don't want further
 * processing of the retrieved data) is with the use of the provided command line tool.
 *
 * By default cmw-cmx-reader will print all metrics/properties on the current system.
 * See the options to filter the output by calling the help (`cmw-cmx-reader --help`).
 *
 * \section CONTROL cmw-cmx-control
 * The control utility is more sophisticated than the simple reader.
 * Invoke cmw-cmx-control -h to see all possible actions.
 *
 * NOTE: The actions will be executed always in the same order as given on the commandline.
 *
 * \section DUMPER cmw-cmx-dumper
 * Dumps data of the CMX C-Structs regardless of any concurrent access.
 *
 */
