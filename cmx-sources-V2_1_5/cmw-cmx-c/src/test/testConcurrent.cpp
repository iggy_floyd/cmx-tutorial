/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file \u201cCOPYING\u201d.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <algorithm>
#include <sstream>
#include <pthread.h>
#include <ctime>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

extern "C"
{
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cmw-cmx/shm.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/shm-private.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/atomic.h>
#include <cmw-cmx/common.h>
#include <cmw-cmx/registry.h>
#include <errno.h>
}

static int run = 0;
static cmx_shm * cmx_shm_ptr;

///////////////////////////////

static int64_t read_count_string = 0;
static int64_t read_fail_string = 0;
static volatile int64_t write_count_string = 0;
static int64_t write_fail_string = 0;
static int value_handle_string;

static char buf[123];
static int bufpos = 0;
static void cb(const char * d, size_t s, void* v)
{
    memcpy(buf + bufpos, d, s);
    bufpos += s;
}

static void * thread_string_read(void *arg)
{
    while (!run)
        sched_yield();
    while (run)
    {
        bufpos = 0;

        read_count_string++;
        if (E_CMX_SUCCESS != cmx_shm_get_value_string(cmx_shm_ptr, value_handle_string, cb, NULL, NULL))
        {
            read_fail_string++;
            continue;
        }
        int l = strlen(buf);
        if (not (buf[0] == 'A' && buf[l - 1] == 'Z') and not (buf[0] == 'Z' && buf[l - 1] == 'A'))
        {
            printf("Failed with value=%s read_count=%"PRId64"\n", buf, read_count_string);
            read_count_string = 0;
            break;
        }
    }
    return NULL;
}

static void * thread_string_write(void *arg)
{
    char buf[123];
    while (!run)
        sched_yield();

    for (int i = 0; i < 5000000; i++)
    {
        write_count_string++;
        buf[0] = (i % 2) ? 'A' : 'Z';
        buf[1] = 'X';
        buf[i % (123 - 4 - 1) + 2] = ('0' + (char) (i % 10));
        buf[(i % (123 - 4 - 1)) + 3] = 'X';
        buf[(i % (123 - 4 - 1)) + 4] = (i % 2) ? 'Z' : 'A';
        buf[(i % (123 - 4 - 1)) + 5] = '\0';

        if (E_CMX_SUCCESS
                != cmx_shm_set_value_string(cmx_shm_ptr, value_handle_string, buf, (i % (123 - 4 - 1)) + 5 + 1))
        {
            write_fail_string++;
            sched_yield();
            continue;
        }

        sched_yield();
    }
    return NULL;
}

TEST(TestConcurrent, strings)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t t_read, t_write1, t_write2;
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("test", &cmx_shm_ptr));
    value_handle_string = cmx_shm_add_value_string(cmx_shm_ptr, "testString", 123);
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_string(cmx_shm_ptr, value_handle_string, "AXXZ", 5));

    run = 0;
    // run 1 reader thread
    assert(0==pthread_create(&t_read,NULL, thread_string_read, NULL));
    // and 2 writer threads
    assert(0==pthread_create(&t_write1, NULL, thread_string_write, NULL));
    assert(0==pthread_create(&t_write2, NULL, thread_string_write, NULL));
    run = 1;
    pthread_join(t_write1, NULL);
    pthread_join(t_write2, NULL);
    run = 0;
    pthread_join(t_read, NULL);

    // in the linux-vm the results are much worse than on real machines.

    EXPECT_GT(read_fail_string, 0); // expect read fails to happen
    EXPECT_LT(read_fail_string / (double )read_count_string, 0.98); // expect read fail of max 98%

    EXPECT_GT(write_fail_string, 0); // expect write fails to happen
    EXPECT_LT(write_fail_string / (double )write_count_string, 0.80); // expect write fail of max 80%

    cmx_shm_destroy(cmx_shm_ptr);
}

/////////////////////

static int value_handle_single;
static int32_t read_count_single = 0;
static int32_t read_fail_single = 0;
static volatile uint16_t write_count_single = 0;
static int32_t write_fail_single = 0;

void * thread_single_read(void *arg)
{
    cmx_shm_value cmx_value;

    while (write_count_single == 0)
        usleep(1000);

    int code;
    while (run)
    {
        read_count_single++;
        switch (code = cmx_shm_get_value_single(cmx_shm_ptr, value_handle_single, NULL, &cmx_value, NULL))
        {
            case E_CMX_CONCURRENT_MODIFICATION:
                read_fail_single++;
                continue;
            case E_CMX_SUCCESS:
                break;
            default:
                std::cout << cmx_common_strerror(code) << std::endl;
                read_count_single = 0;
                return NULL;
        }
        uint16_t z = (uint16_t) cmx_value._int64;
        uint16_t w = write_count_single;
        if ((uint16_t) (w - z) > 20 * 1000) // assert that the value is not older then 20k txns
        {
            printf("Write count single=%d cmx_value._int64=(uint16_t)%d\n", w, z);
            read_count_single = 0;
            break;
        }
    }
    return NULL;
}

static void * thread_single_write(void *arg)
{
    cmx_shm_value cmx_value;
    int code;
    while (!run)
        sched_yield();

    for (int i = 0; i < 5000000; i++)
    {
        write_count_single++;
        cmx_value._int64 = write_count_single;

        switch (code = cmx_shm_set_value_single(cmx_shm_ptr, value_handle_single, CMX_TYPE_INT64, &cmx_value))
        {
            case E_CMX_CONCURRENT_MODIFICATION:
                write_fail_single++;
                sched_yield();
                continue;
            case E_CMX_SUCCESS:
                break;
            default:
                std::cout << cmx_common_strerror(code) << std::endl;
                write_count_single = 0;
                return NULL;
        }
        sched_yield();
    }
    return NULL;
}

TEST(TestConcurrent, single)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t t_read, t_write;
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("test", &cmx_shm_ptr));
    value_handle_single = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "testsingle");

    run = 0;
    // run 1 reader thread
    assert(0==pthread_create(&t_read,NULL, thread_single_read, NULL));
    // and 2 writer threads
    assert(0==pthread_create(&t_write, NULL, thread_single_write, NULL));
    run = 1;
    pthread_join(t_write, NULL);
    run = 0;
    pthread_join(t_read, NULL);

    // in the linux-vm the results are much worse than on real machines.
    EXPECT_GT(read_count_single, 0) << "if == 0 then thread_single_read aborted with error";
    EXPECT_GT(write_count_single, 0) << "if == 0 then thread_single_write aborted with error";
    EXPECT_GT(read_fail_single / (double )read_count_single, 0.000001); // expect read fail of min 0.0001%
    EXPECT_LT(read_fail_single / (double )read_count_single, 0.98); // expect read fail of max 98%

    EXPECT_EQ(0, write_fail_single); // 1 threead -> no failures possible
    cmx_shm_destroy(cmx_shm_ptr);
}

//////////////////////////////////

static int thread_single2_read_failed = 1;
static int thread_single2_write_failed = 1;
static int value_handle_single2 = 0;

static void * thread_single2_read(void *arg)
{
    cmx_shm_value cmx_value;
    int cmx_type;
    uint64_t cmx_mtime;
    uint64_t txn;

    while (!run)
        sched_yield();

    usleep(1000); // give the writers some time to speed up

    while (run)
    {
        switch (int code = cmx_shm_get_value_single(cmx_shm_ptr, value_handle_single2, &cmx_type, &cmx_value, &cmx_mtime))
        {
            case E_CMX_CONCURRENT_MODIFICATION:
            sched_yield();
            continue;
            case E_CMX_SUCCESS:
            break;
            default:
            std::cout << cmx_common_strerror(code ) << std::endl;
            return NULL;
        }
        txn = cmx_atomic_read_uint64(& (cmx_shm_ptr->value[cmx_shm_unpack_index(value_handle_single2)].value.txn));
        if (cmx_type != CMX_TYPE_INT64)
        {
            std::cout << "type mismatch" << std::endl;
            return NULL;
        }
        if (txn < (uint64_t)cmx_value._int64)
        {
            std::cout << "txn constraint1 txn=" << txn << " _int64="<< cmx_value._int64 << std::endl;
            return NULL;
        }
        if ((txn - cmx_value._int64) > 1500)
        {
            // this is not directly an error. for now only log it.
            // given the load of the machine and the number of processors it can be perfectly ok
            // that the reader thread stalls between reading the value and the txn manually
            std::cout << "WARNING missed txn > 1500: delta-txn=" << (txn - cmx_value._int64) << "  txn=" << txn << std::endl;
        }
    }
    thread_single2_read_failed = 0;
    return NULL;
}

static void * thread_single2_write(void *arg)
{
    cmx_shm_value cmx_value;
    while (!run)
        sched_yield();

    for (int i = 0; i < 2000000; i++)
    {
        cmx_value._int64 = (int64_t) cmx_shm_ptr->value[cmx_shm_unpack_index(value_handle_single2)].value.txn + 1;
        switch (int code = cmx_shm_set_value_single(cmx_shm_ptr, value_handle_single2, CMX_TYPE_INT64, &cmx_value))
        {
            case E_CMX_CONCURRENT_MODIFICATION:
            sched_yield();
            continue;
            case E_CMX_SUCCESS:
            break;
            default:
            std::cout << cmx_common_strerror(code) << std::endl;
            return NULL;
        }
    }
    thread_single2_write_failed = 0;
    return NULL;
}

TEST(TestConcurrent, single2)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t t_read, t_write1, t_write2;
    {
        char buf[1024];
        struct stat s;
        cmx_registry_format_filename(buf, getpid(), "test");
        EXPECT_EQ(-1, stat(buf, &s));
        EXPECT_EQ(ENOENT, errno);
    }
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("test", &cmx_shm_ptr));
    value_handle_single2 = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "test");
    ASSERT_TRUE(value_handle_single2 > 0);

    run = 0;
    // run 1 reader thread
    assert(0==pthread_create(&t_read,NULL, thread_single2_read, NULL));
    // and 2 writer threads
    assert(0==pthread_create(&t_write1, NULL, thread_single2_write, NULL));
    assert(0==pthread_create(&t_write2, NULL, thread_single2_write, NULL));
    run = 1;
    pthread_join(t_write1, NULL);
    pthread_join(t_write2, NULL);
    run = 0;
    pthread_join(t_read, NULL);

    EXPECT_EQ(0, thread_single2_read_failed);
    EXPECT_EQ(0, thread_single2_write_failed);

    {
        char buf[1024];
        struct stat s;
        cmx_registry_format_filename(buf, getpid(), "test");
        EXPECT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
        EXPECT_EQ(-1, stat(buf, &s));
        EXPECT_EQ(ENOENT, errno);
    }
}

/**
 * Test Description:
 *   2 Writer, 2 Reader.
 *   The Writers continously updating a single cmx field with a value containing a checksum.
 *   The Reader reads this value and validates the checksum.
 * Expectation:
 *   If data corruption happens between the two writers the checksum is invalid.
 */

static int strings2_value_handle = 0;
static int strings2_read_invalid = 0;
static int strings2_read_valid = 0;
static int strings2_write_success = 0;
static int strings2_write_failed = 0;
static int strings2_run_reader = 0;
static const int strings2_string_size = 512;

struct strings2_writer_opts
{
    unsigned int seed;
};

void * thread_strings2_writer(void * user_arg)
{
    struct strings2_writer_opts * opts = (struct strings2_writer_opts *) user_arg;
    char buf[strings2_string_size];

    while (!run)
        usleep(1000);

    while (cmx_atomic_read_int32(&run))
    {
        // mutate buf
        int r = rand_r(&opts->seed);
        int chksum = 0;
        for (int i = 0; i < strings2_string_size - 1; i++)
        {
            int v = buf[i];
            v = v / (i + 1);
            v += r;
            buf[i] = (char) v;
            chksum = (char) (chksum + buf[i]);
        }
        // checksum
        buf[strings2_string_size - 1] = chksum;
        // set
        if (E_CMX_SUCCESS == cmx_shm_set_value_string(cmx_shm_ptr, strings2_value_handle, buf, strings2_string_size))
        {
            strings2_write_success++;
        }
        else
        {
            strings2_write_failed++;
        }

        cmx_atomic_set_int32(&strings2_run_reader, 1); // enable reader
        sched_yield();
    }
    return NULL;
}

struct strings2_reader_state
{
    bool test;
    char chksum;
    int bytes_read;
    char last_char;
};

void strings2_reader_checksum_cb(const char * buf, const size_t len, void * userarg)
{
    struct strings2_reader_state * rs = (struct strings2_reader_state*) userarg;
    rs->test = true;
    for (size_t i = 0; i < len && rs->bytes_read < strings2_string_size - 1; i++)
    {
        // same as strings2_simplehash()
        rs->chksum = (char) (rs->chksum + buf[i]);
        rs->bytes_read++;
    }
    if (len > 0) rs->last_char = buf[len - 1];
    if (rs->bytes_read == 0)
    {
        std::cout << "rs->bytes_read =" << rs->bytes_read << std::endl;
    }
}

void * thread_strings2_reader(void * user_arg)
{
    struct strings2_reader_state reader_state;
    while (!strings2_run_reader)
        usleep(1000);

    while (cmx_atomic_read_int32(&run))
    {
        reader_state.test = false;
        reader_state.chksum = 0;
        cmx_atomic_set_int32(&reader_state.bytes_read, 0);
        reader_state.last_char = 0;
        int r = cmx_shm_get_value_string(cmx_shm_ptr, strings2_value_handle, //
                strings2_reader_checksum_cb, &reader_state, NULL);
        if (r == E_CMX_CONCURRENT_MODIFICATION) continue;
        if (r != E_CMX_SUCCESS)
        {
            strings2_read_valid = 0;
            std::cout << "Failed with " << cmx_common_strerror(r) << std::endl;
            return NULL;
        }

        if (!reader_state.test && reader_state.chksum == 0 && reader_state.bytes_read == 0
                && reader_state.last_char == 0)
        {
            strings2_read_valid = 0;
            std::cout << "all null " << r << std::endl;
        }

        if (reader_state.chksum == reader_state.last_char && reader_state.bytes_read == strings2_string_size - 1)
        {
            strings2_read_valid++;
        }
        else
        {
            std::cout << "reader_state.chksum=" << (int) reader_state.chksum << "  reader_state.last_char="
                    << (int) reader_state.last_char << std::endl;
            std::cout << "reader_state.bytes_read=" << reader_state.bytes_read << " strings2_string_size - 1="
                    << (strings2_string_size - 1) << std::endl;
            strings2_read_invalid++;
        }
    }
    return NULL;
}

TEST(TestConcurrent, strings2)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t writer1, writer2, reader;
    struct strings2_writer_opts writer1_opts, writer2_opts;
    writer1_opts.seed = (unsigned int) time(NULL);
    writer2_opts.seed = ((unsigned int) time(NULL)) % 1234;

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("TestConcurrent.strings2", &cmx_shm_ptr));
    strings2_value_handle = cmx_shm_add_value_string(cmx_shm_ptr, "test", strings2_string_size);
    ASSERT_TRUE(strings2_value_handle > 0);
    cmx_atomic_set_int32(&run, 0);
    pthread_create(&reader, NULL, thread_strings2_reader, NULL);
    pthread_create(&writer1, NULL, thread_strings2_writer, &writer1_opts);
    pthread_create(&writer2, NULL, thread_strings2_writer, &writer2_opts);
    cmx_atomic_set_int32(&run, 1);
    sleep(5);
    cmx_atomic_set_int32(&run, 0);
    pthread_join(writer1, NULL);
    pthread_join(writer2, NULL);
    pthread_join(reader, NULL);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
    EXPECT_EQ(0, strings2_read_invalid) << "strings2_read_invalid > 0 means data-corruption";
    EXPECT_GT(strings2_read_valid, 250000/20) << "reader made no progress strings2_writes=" << strings2_write_success;
    EXPECT_GT(strings2_write_success, 250000) << "writer made no progress strings2_read_valid=" << strings2_read_valid;
    // it IS possible that there are no writer-fails if the system is under heavy load and only one writer thread
    // can be active at any time. So it makes no sense to check if there are at least some writer-fails
}

/**
 * Test strings3: 2 threads adding/removing string values
 */
int strings3_start = 0;

void * thread_strings3_writer(void * user_arg)
{
    char * value_name = (char*) alloca(CMX_NAME_SZ);
    char * value_name_buf = (char*) alloca(CMX_NAME_SZ);
    int * fail_writerN = (int*) user_arg;
    int r = 0;
    int string_metric;

    while (!cmx_atomic_read_int32(&strings3_start))
        sched_yield();

    for (int i = 0; i < CMX_NUMBER_OF_VALUES * 8; ++i)
    {
        //add
        snprintf(value_name, CMX_NAME_SZ, "thread-%ld-%d", (long int) pthread_self(), i);
        r = string_metric = cmx_shm_add_value_string(cmx_shm_ptr, value_name, 8);
        if (!r > 0) goto out_error;

        //set
        if (E_CMX_SUCCESS != (r = cmx_shm_set_value_string(cmx_shm_ptr, string_metric, "12345678", 8))) goto out_error;

        // check name
        if (E_CMX_SUCCESS != (r = cmx_shm_get_value_name(cmx_shm_ptr, string_metric, value_name_buf))) goto out_error;
        if (strncmp(value_name, value_name_buf, CMX_NAME_SZ) != 0) goto out_error;

        //remove
        if (E_CMX_SUCCESS != (r = cmx_shm_remove_value(cmx_shm_ptr, string_metric))) goto out_error;
    }
    return NULL;
    out_error: //
    (*fail_writerN)++;
    std::cout << "Error: " << cmx_common_strerror(r) << std::endl;
    return NULL;
}

TEST(TestConcurrent, strings3)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t writer1 = 0, writer2 = 0;
    int fail_writer1 = 0, fail_writer2 = 0, temp;

    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    pthread_create(&writer1, NULL, thread_strings3_writer, &fail_writer1);
    pthread_create(&writer2, NULL, thread_strings3_writer, &fail_writer2);
    cmx_atomic_smp_mb();
    cmx_atomic_set_int32(&strings3_start, 1);
    cmx_atomic_smp_wmb();
    pthread_join(writer1, NULL);
    pthread_join(writer2, NULL);

    ASSERT_EQ(E_CMX_NOT_FOUND, cmx_shm_find_value(cmx_shm_ptr, 0, &temp, &temp));

    ASSERT_EQ(0, fail_writer1);
    ASSERT_EQ(0, fail_writer2);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

/**
 * Same as strings2 but with INT64. However ints are a bit different. The value itself will be written
 * atomic. However bad things may happen, so we are essentially only testing for memory corruption here.
 */
static int single3_value_handle = 0;
static int single3_writes = 0;
static int single3_run_reader = 0;
static int single3_read_valid = 0;
static int single3_read_invalid = 0;

struct single3_writer_opts
{
    unsigned int seed;
};

void * thread_single3_writer(void * user_arg)
{
    struct single3_writer_opts * opts = (struct single3_writer_opts *) user_arg;
    cmx_shm_value shm_value;

    while (!run)
        usleep(1000);

    while (cmx_atomic_read_int32(&run))
    {
        // mutate value
        int r = rand_r(&opts->seed);
        shm_value._int64 = (shm_value._int64 ^ r) && 0xfffffffe; // mutate value
        if (shm_value._int64 % 2 == 0) // if % 2 ==0 then set lowest bit
        { // set "checksum" bit {
            shm_value._int64 |= 0xf;
        }

        // set
        cmx_shm_set_value_single(cmx_shm_ptr, single3_value_handle, CMX_TYPE_INT64, &shm_value);
        single3_writes++;

        cmx_atomic_set_int32(&single3_run_reader, 1); // enable reader
        sched_yield();
    }
    return NULL;
}

void * thread_single3_reader(void * user_arg)
{
    cmx_shm_value shm_value;
    int type;
    while (!single3_run_reader)
        usleep(1000);

    while (cmx_atomic_read_int32(&run))
    {
        int r = cmx_shm_get_value_single(cmx_shm_ptr, single3_value_handle, &type, &shm_value, NULL);
        if (r == E_CMX_CONCURRENT_MODIFICATION) continue;
        if (r != E_CMX_SUCCESS)
        {
            strings2_read_valid = 0;
            std::cout << "Failed with " << cmx_common_strerror(r) << std::endl;
            return NULL;
        }
        if (type != CMX_TYPE_INT64)
        {

            strings2_read_valid = 0;
            std::cout << "Failed with type error " << std::endl;
            return NULL;
        }

        int v = shm_value._int64;
        // test the "checksum"
        if ((v & 0xfffffffe) % 2 == 0 && (v & 0x1))
        {
            single3_read_valid++;
        }
        else if (((v & 0xfffffffe) % 2 == 0 && (v & 0x1) == 0))
        {
            single3_read_valid++;
        }
        else
        {
            single3_read_invalid++;
        }
    }
    return NULL;
}

TEST(TestConcurrent, single3_int)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t writer1, writer2, reader;
    struct strings2_writer_opts writer1_opts, writer2_opts;
    writer1_opts.seed = (unsigned int) time(NULL);
    writer2_opts.seed = ((unsigned int) time(NULL)) % 1234;

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("TestConcurrent.single3", &cmx_shm_ptr));
    single3_value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "testint");
    ASSERT_TRUE(single3_value_handle > 0);

    cmx_atomic_set_int32(&run, 0);
    pthread_create(&reader, NULL, thread_single3_reader, NULL);
    pthread_create(&writer1, NULL, thread_single3_writer, &writer1_opts);
    pthread_create(&writer2, NULL, thread_single3_writer, &writer2_opts);
    cmx_atomic_set_int32(&run, 1);
    sleep(5);
    cmx_atomic_set_int32(&run, 0);
    pthread_join(writer1, NULL);
    pthread_join(writer2, NULL);
    pthread_join(reader, NULL);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
    EXPECT_EQ(0, single3_read_invalid) << "strings2_read_invalid > 0 means data-corruption";
    EXPECT_GT(single3_read_valid, 25000) << "reader made no progress";
    EXPECT_GT(single3_writes, 250000) << "writer made no progress";
}

/**
 * Test single4: 2 threads adding/removing string values
 * Same as strings3 but with int64
 */
int single4_start = 0;

void * thread_single4_writer(void * user_arg)
{
    char * value_name = (char*) alloca(CMX_NAME_SZ);
    char * value_name_buf = (char*) alloca(CMX_NAME_SZ);
    int * fail_writerN = (int*) user_arg;
    int r = 0;
    int int64_metric;
    cmx_shm_value shm_value;

    while (!cmx_atomic_read_int32(&single4_start))
        sched_yield();

    for (int i = 0; i < CMX_NUMBER_OF_VALUES * 8; ++i)
    {
        //add
        snprintf(value_name, CMX_NAME_SZ, "thread-%ld-%d", (long int) pthread_self(), i);
        r = int64_metric = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, value_name);
        if (!r > 0) goto out_error;

        //set
        shm_value._int64 = i;
        if (E_CMX_SUCCESS != (r = cmx_shm_set_value_single(cmx_shm_ptr, int64_metric, CMX_TYPE_INT64, &shm_value))) goto out_error;

        // check name
        if (E_CMX_SUCCESS != (r = cmx_shm_get_value_name(cmx_shm_ptr, int64_metric, value_name_buf))) goto out_error;
        if (strncmp(value_name, value_name_buf, CMX_NAME_SZ) != 0) goto out_error;

        //remove
        if (E_CMX_SUCCESS != (r = cmx_shm_remove_value(cmx_shm_ptr, int64_metric))) goto out_error;
    }
    return NULL;
    out_error: //
    (*fail_writerN)++;
    std::cout << "Error: " << cmx_common_strerror(r) << std::endl;
    return NULL;
}

TEST(TestConcurrent, single4)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t writer1 = 0, writer2 = 0;
    int fail_writer1 = 0, fail_writer2 = 0, temp;

    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    pthread_create(&writer1, NULL, thread_single4_writer, &fail_writer1);
    pthread_create(&writer2, NULL, thread_single4_writer, &fail_writer2);
    cmx_atomic_smp_mb();
    cmx_atomic_set_int32(&single4_start, 1);
    cmx_atomic_smp_wmb();
    pthread_join(writer1, NULL);
    pthread_join(writer2, NULL);

    ASSERT_EQ(E_CMX_NOT_FOUND, cmx_shm_find_value(cmx_shm_ptr, 0, &temp, &temp));

    ASSERT_EQ(0, fail_writer1);
    ASSERT_EQ(0, fail_writer2);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

/**
 *
 */
int remove_start = 0;
int remove_fail = 0;

void * thread_remove_writer(void * threadarg)
{
    for (int i = 0; i < 100000; i++)
    {
        int metric_index = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "TEST");
        if (metric_index < 0)
        {
            printf("%s", cmx_common_strerror(metric_index));
            remove_fail |= 1;
            return NULL;
        }
        {
            cmx_shm_value v;
            v._int64 = 1234;
            cmx_shm_set_value_single(cmx_shm_ptr, metric_index, CMX_TYPE_INT64, &v);
        }
        int r = cmx_shm_remove_value(cmx_shm_ptr, metric_index);
        if (r != E_CMX_SUCCESS)
        {
            printf("%s", cmx_common_strerror(metric_index));
            remove_fail |= 1;
            return NULL;
        }
    }
    return NULL;
}

void * thread_remove_reader(void * threadarg)
{
    return NULL;
}

TEST(TestConcurrent, remove)
{
    if (getenv("LD_PRELOAD") != NULL)
    {
        if (std::string(getenv("LD_PRELOAD")).find("valgrind") != std::string::npos)
        {
            std::cout << "This test is disabled under valgrind" << std::endl;
            return;
        }
    }
    pthread_t writer1 = 0, writer2 = 0;

    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    pthread_create(&writer1, NULL, thread_remove_writer, NULL);
    pthread_create(&writer2, NULL, thread_remove_reader, NULL);
    cmx_atomic_smp_mb();
    cmx_atomic_set_int32(&remove_start, 1);
    cmx_atomic_smp_wmb();
    pthread_join(writer1, NULL);
    pthread_join(writer2, NULL);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));

    ASSERT_NE(1, remove_fail);
}
