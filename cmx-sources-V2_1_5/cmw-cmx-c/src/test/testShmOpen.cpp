/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file \u201cCOPYING\u201d.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <sstream>
#include <csetjmp>

extern "C"
{
#include <cmw-cmx/shm.h>
#include <cmw-cmx/log.h>
}

#include "gtest-segv.h"

///////////////////////////////

TEST(TestShmOpen, create_open)
{
    cmx_shm * cmx_shm_ptr_creater;
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr_creater));

    {
        cmx_shm * cmx_shm_ptr_reader;
        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_open(getpid(), name.c_str(), &cmx_shm_ptr_reader));
        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_unmap(cmx_shm_ptr_reader));
    }

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr_creater));
}

TEST(TestShmOpen, open_invalid_arg)
{
    char buf[CMX_NAME_SZ + 1];
    memset(buf, 'a', CMX_NAME_SZ);
    buf[CMX_NAME_SZ] = '\0';

    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
    {
        ASSERT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_open(getpid(), "", NULL));
        ASSERT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_open(getpid(), "NAME", NULL));
        ASSERT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_open(getpid(), buf, (cmx_shm ** )0x1));
    }
}


TEST(TestShmOpen, open_ro_writefail)
{
    const int64_t MAGIC = 123462342341231LL;
    cmx_shm * cmx_shm_ptr_creater;
    cmx_shm_value shm_value;
    int test_value_handle;
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr_creater));
    ASSERT_GT(test_value_handle = cmx_shm_add_value_single(cmx_shm_ptr_creater, CMX_TYPE_INT64, "TEST"), 0);
    shm_value._int64 = MAGIC;
    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_set_value_single(cmx_shm_ptr_creater, test_value_handle, CMX_TYPE_INT64, &shm_value));

    {
        cmx_shm * cmx_shm_ptr_reader;
        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_open_ro(getpid(), name.c_str(), &cmx_shm_ptr_reader));

        EXPECT_EQ(E_CMX_SUCCESS,
                cmx_shm_get_value_single(cmx_shm_ptr_reader, test_value_handle, NULL, &shm_value, NULL));
        EXPECT_EQ(MAGIC, shm_value._int64);

        ASSERT_SEGV(cmx_shm_set_value_single(cmx_shm_ptr_reader, test_value_handle, CMX_TYPE_INT64, &shm_value))<< "Setting value in read-only must segfault";
        ASSERT_SEGV_HAPPENED_BETWEEN((char*) cmx_shm_ptr_reader, ((char*)cmx_shm_ptr_reader) + 0x100)<< "Segv happened at a unexpected memory address";

        ASSERT_SEGV(cmx_shm_add_value_single(cmx_shm_ptr_reader, CMX_TYPE_INT64, "OtherTest"))<< "Adding value to read-only cmx component succeeded";
        ASSERT_SEGV_HAPPENED_BETWEEN((char*) cmx_shm_ptr_reader, ((char*)cmx_shm_ptr_reader) + 0x100)<< "Segv happened at a unexpected memory address";

        ASSERT_SEGV(cmx_shm_remove_value(cmx_shm_ptr_reader, test_value_handle))<< "Removing a value in read-only cmx_shm must segfault";
        ASSERT_SEGV_HAPPENED_BETWEEN((char*) cmx_shm_ptr_reader, ((char*)cmx_shm_ptr_reader) + 0x100)<< "Segv happened at a unexpected memory address";

        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_unmap(cmx_shm_ptr_reader));
    }

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr_creater));
}
