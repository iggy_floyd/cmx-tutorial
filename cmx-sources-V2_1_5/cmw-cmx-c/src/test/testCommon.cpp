/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/*
 * \internal
 * \file
 */
#include <gtest/gtest.h>

extern "C"
{
#include <cmw-cmx/common.h>
#include <cmw-cmx/log.h>
}

// EXECUTE-LCOV: TestCommon.* */src/cmw-cmx/common.c

TEST(TestCommon, strerror)
{
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_SUCCESS));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_OPERATION_FAILED));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_INVALID_HANDLE));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_TYPE_MISMATCH));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_CORRUPT_SEGMENT));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_INVALID_ARGUMENT));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_OUT_OF_MEMORY));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_CREATE_FAILED));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_INVALID_PID));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_CONCURRENT_MODIFICATION));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_NAME_EXISTS));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_NOT_FOUND));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_COMPONENT_FULL));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_PROTOCOL_VERSION));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_OUT_OF_RANGE));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_UNUSED_3));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_UNUSED_2));
    EXPECT_TRUE(NULL != cmx_common_strerror(E_CMX_UNUSED_1));
    //
    EXPECT_TRUE(NULL == cmx_common_strerror(999));
}

TEST(TestCommon,check_string_length)
{
    // NULL-String
    EXPECT_EQ(CMX_FALSE, cmx_common_check_string_length(NULL, 1,2));
    // less-than minimum length string
    EXPECT_EQ(CMX_FALSE, cmx_common_check_string_length("test", 5, 10));
    EXPECT_EQ(CMX_FALSE, cmx_common_check_string_length("", 1, 10));
    // more-than maximum length string
    EXPECT_EQ(CMX_FALSE, cmx_common_check_string_length("test", 1, 2));
    EXPECT_EQ(CMX_FALSE, cmx_common_check_string_length("aaaa", 0, 4));
    EXPECT_EQ(CMX_FALSE, cmx_common_check_string_length("aaaaa", 0, 4));
    // positive tests
    EXPECT_EQ(CMX_TRUE, cmx_common_check_string_length("aaa", 0, 4));
    EXPECT_EQ(CMX_TRUE, cmx_common_check_string_length("a", 0, 4));
    EXPECT_EQ(CMX_TRUE, cmx_common_check_string_length("", 0, 4));

    EXPECT_EQ(CMX_TRUE, cmx_common_check_string_length("aaa", 1, 4));
    EXPECT_EQ(CMX_TRUE, cmx_common_check_string_length("a", 1, 4));
}

TEST(TestCommon,current_pid)
{
    ASSERT_EQ(getpid(), cmx_common_current_pid());
}

TEST(TestCommon, current_time_usec__deviation)
{
    // check that the clock is moving forward and in conformance with sleep().
    unsigned long long time1 = cmx_common_current_time_usec();
    sleep(1);
    unsigned long long time2 = cmx_common_current_time_usec();
    EXPECT_LT(time1, time2);
    EXPECT_NEAR(1000ULL * 1000ULL, abs((int )(time2 - time1)), 7000); //tolerate 7ms deviation. VM's are bad at this
    // especially when running with valgrind
}

TEST(TestCommon, current_time_usec__notNull)
{
    ASSERT_NE(0U, cmx_common_current_time_usec());
}
