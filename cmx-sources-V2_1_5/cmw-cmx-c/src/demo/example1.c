/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <cmw-cmx/cmx.h>

int main()
{
    cmx_shm * cmx_shm_ptr;
    cmx_shm_value cmx_value;

    struct timespec tm =
    { .tv_sec = 0, .tv_nsec = 50000000 };

    printf("Register process\n");
    cmx_process_update();

    printf("Create component\n");
    assert(E_CMX_SUCCESS == cmx_shm_create("stats", &cmx_shm_ptr));

    printf("Add metric\n");
    int metr_test = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "test");
    assert(metr_test >= 0);

    printf("Enter work-sleep loop\n ");
    for (int i = 0; i < 100; i++)
    {
        // update metric
        cmx_value._int64 = i;
        assert(E_CMX_SUCCESS == cmx_shm_set_value_single(cmx_shm_ptr, metr_test, CMX_TYPE_INT64, &cmx_value));

        if (i % 500 == 0) cmx_process_update(); // update process metrics

        printf(".");
        fflush(stdout);
        nanosleep(&tm, NULL);
    }

    printf("\n Remove test component\n");
    assert(E_CMX_SUCCESS == cmx_shm_destroy(cmx_shm_ptr));
}
