/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>
#include <getopt.h>
#include <stdlib.h>
#include <cmw-cmx/cmx.h>

static cmx_shm * cmx_shm_ptr_ro;
static int value_handle;
static int value_type;

static void reader(void)
{
    long int count_success = 0;
    long int count_fail = 0;
    time_t last_time = 0UL;
    uint64_t mtime;
    cmx_shm_value value;
    int type;
    int r;
    while (1)
    {
        r = cmx_shm_get_value_single(cmx_shm_ptr_ro, value_handle, &type, &value, &mtime);
        switch (r)
        {
            case E_CMX_SUCCESS:
                count_success++;
                break;
            case E_CMX_CONCURRENT_MODIFICATION:
                count_fail++;
                break;
            default:
                fprintf(stderr, "%s\n", cmx_common_strerror(r));
                assert(("Read failed", r != E_CMX_SUCCESS));
                exit(1);
        }
        if ((count_fail + count_success) % 1000 == 0)
        {
            if ((last_time + 5) - time(NULL) < 0)
            {
                printf("success=%ld=%ld K/s,fail=%ld=%ld K/s\n", count_success / 5, count_success / 5000UL,
                        count_fail / 5, count_fail / 5000UL);
                last_time = time(NULL);
                count_fail = 0;
                count_success =0;
            }
        }
    }
}

static void help()
{
    printf(" -p <pid> -c <component-name>\n");
}

int main(int argc, char * argv[])
{
    int opt;
    int pid = -1;
    const char * component_name = NULL;

    while ((opt = getopt(argc, argv, "p:c:h")) != -1)
    {
        switch (opt)
        {
            case 'p':
                pid = atoi(optarg);
                break;
            case 'c':
                component_name = optarg;
                break;
            case 'h':
            default:
                help();
                break;
        }
    }
    if (pid == -1 || component_name == NULL)
    {
        help();
        return 1;
    }

    assert(E_CMX_SUCCESS == cmx_shm_open_ro(pid, component_name, &cmx_shm_ptr_ro));

    assert(E_CMX_SUCCESS == cmx_shm_find_value(cmx_shm_ptr_ro, 0, &value_handle, &value_type));

    reader();

    assert(E_CMX_SUCCESS == cmx_shm_unmap(cmx_shm_ptr_ro));
    return 0;
}
