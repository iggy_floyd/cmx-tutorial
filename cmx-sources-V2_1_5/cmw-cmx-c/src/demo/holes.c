/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <string.h>
#include <cmw-cmx/cmx.h>
#include <pthread.h>
#include <sys/resource.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/shm-private.h>

#define MAX_NO_METRICS 15000

static cmx_shm * cmx_shm_ptr;
static long maxrss_init;
static struct rusage ru;

static void _getrusage()
{
    assert(0==getrusage(RUSAGE_SELF, &ru));
}

// minflt %lu  The  number of minor faults the process has made which have not required loading a
//             memory page from disk.
//rss %ld     Resident Set Size: number of pages the process has in real memory.  This  is  just
//            the  pages  which count towards text, data, or stack space.  This does not include
//            pages which have not been demand-loaded in, or which are swapped out.

void print_usage(int no_metrics)
{
    _getrusage();
    printf("{\"no_metrics\":%d, \"ru_minflt\":%ld, ", no_metrics, ru.ru_minflt);
    printf("\"calculated_value_slot_size\":%ld, \"maxrss\":%ld, \"maxrss-minus-init\":%ld}\n",
            (sizeof(cmx_shm_slot_value) * no_metrics / 1000), ru.ru_maxrss, ru.ru_maxrss - maxrss_init);
}

int main()
{
    cmx_process_update();

    // create shm
    assert(("failed to create cmx shm", cmx_shm_create("test", &cmx_shm_ptr) == E_CMX_SUCCESS));

    _getrusage();
    maxrss_init = ru.ru_maxrss;

    // add metrics
    char name[64];
    for (int i = 0; i < MAX_NO_METRICS; i++)
    {
        snprintf(name, 64, "m-%d", i);
        assert(("Assert add_value > 0", (cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, name) > 0)));
        print_usage(i + 1);
    }

    assert(("failed to destroy cmx shm", E_CMX_SUCCESS == cmx_shm_destroy(cmx_shm_ptr)));

    return 0;
}
