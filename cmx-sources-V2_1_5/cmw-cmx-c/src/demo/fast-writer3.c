/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>
#include <getopt.h>
#include <stdlib.h>
#include <cmw-cmx/cmx.h>

static long stoptime = 0;
static int no_of_readers = -1;

static cmx_shm * cmx_shm_ptr;
static cmx_shm * cmx_shm_ptr_ro;
static int value_handle;

static void * writer(void * arg)
{
    cmx_shm_value value;
    long counter = 1;
    uint64_t timer = cmx_common_current_time_usec();
    while (time(NULL) < stoptime)
    {
        if (counter % 50 * 1000 == 0)
        {
            if (cmx_common_current_time_usec() - timer > 1000000)
            {
                printf("CMX_TYPE_INT64,updates-per-second,%u,%uK,no-readers,%d\n", counter, counter / 1000,
                        no_of_readers);
                counter = 0;
                timer = cmx_common_current_time_usec();
            }
        }
        value._int64 = (int64_t) counter;
        assert(("Write failed", //
        /*   */E_CMX_SUCCESS == cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_INT64, &value)));
        counter++;
    }
    return NULL;
}

static void * reader(void * arg)
{
    uint64_t mtime;
    cmx_shm_value value;
    int type;
    int r;
    while (time(NULL) < stoptime)
    {
        r = cmx_shm_get_value_single(cmx_shm_ptr_ro, value_handle, &type, &value, &mtime);
        if (r != E_CMX_SUCCESS)
        {
            fprintf(stderr, "%s\n", cmx_common_strerror(r));
            assert(("Read failed", r != E_CMX_SUCCESS));
        }
    }
    return NULL;
}

static void help()
{
    printf(" -r <num of reader threads> -t <number of seconds>\n");
}

int main(int argc, char * argv[])
{
    int opt;

    while ((opt = getopt(argc, argv, "r:t:h")) != -1)
    {
        switch (opt)
        {
            case 'r':
                no_of_readers = atoi(optarg);
                break;
            case 't':
                stoptime = time(NULL) + atoi(optarg);
                break;
            case 'h':
            default:
                help();
                break;
        }
    }
    if (stoptime == 0 || no_of_readers == -1)
    {
        help();
        return 1;
    }

    cmx_process_update();
    assert(E_CMX_SUCCESS == cmx_shm_create("fast-writer", &cmx_shm_ptr));
    value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "TestInt64");
    assert(value_handle > 0);

    assert(E_CMX_SUCCESS == cmx_shm_open_ro(getpid(), "fast-writer", &cmx_shm_ptr_ro));

    {
        pthread_t writer_thread;
        pthread_t reader_thread[no_of_readers];

        pthread_create(&writer_thread, NULL, writer, NULL);
        for (int i = 0; i < no_of_readers; i++)
        {
            pthread_create(&reader_thread[i], NULL, reader, NULL);
        }

        pthread_join(writer_thread, NULL);
        for (int i = 0; i < no_of_readers; i++)
        {
            pthread_join(reader_thread[i], NULL);
        }
    }
    assert(E_CMX_SUCCESS == cmx_shm_destroy(cmx_shm_ptr));
    return 0;
}
