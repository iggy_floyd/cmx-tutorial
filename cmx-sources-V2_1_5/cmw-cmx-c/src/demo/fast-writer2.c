/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include <cmw-cmx/shm.h>
#include <cmw-cmx/process.h>

int main()
{
    cmx_process_update();
    uint64_t time = cmx_common_current_time_usec();
    uint32_t counter = 0;
    cmx_shm * cmx_shm_ptr = NULL;
    cmx_shm_value value;
    int r;
    int value_handle;

    assert(E_CMX_SUCCESS == cmx_shm_create("fast-writer", &cmx_shm_ptr));
    value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "TestInt64");
    assert(value_handle > 0);

    for (int i = 0; i < 1000000; i++)
    {
        if (counter % 50 * 1000 == 0)
        {
            if (cmx_common_current_time_usec() - time > 1000000)
            {
                printf("%u = %uK CMX_TYPE_INT64 updates/s\n", counter, counter / 1000);
                counter = 0;
                time = cmx_common_current_time_usec();
            }
        }
        value._int64 = (int64_t) counter;
        r = cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_INT64, &value);
        if (E_CMX_SUCCESS != r)
        {
            printf("Write failed: %s", cmx_common_strerror(r));
        }
        else
        {
            counter++;
        }
    }

    cmx_shm_destroy(cmx_shm_ptr);
    return 0;
}
