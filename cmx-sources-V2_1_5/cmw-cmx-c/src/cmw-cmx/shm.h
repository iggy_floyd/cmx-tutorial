/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/** \file
 *  \brief Shared Memory CMX Component, public API
 */
#ifndef HAVE_CMW_CMX_C_SHM_H
#define HAVE_CMW_CMX_C_SHM_H
#include <cmw-cmx/common.h>

typedef enum cmx_value_type_t
{
    CMX_TYPE_INT64 = 1, CMX_TYPE_FLOAT64 = 2, CMX_TYPE_BOOL = 3, CMX_TYPE_STRING = 4
} cmx_value_type;

/**
 * \internal
 * \brief String size/next-handle for value_slot
 */
typedef struct cmx_shm_value_string_t
{
    /// size of this string slot incl. null-termination
    uint16_t size;
    /// size of current value of this string slot incl. null-termination
    uint16_t current_size;
    /// index of the first payload slot
    uint16_t next_index;
    /// id of the first payload slot
    uint16_t next_id;
} cmx_shm_value_string;

/**
 * \brief Union for single-slot values.
 *
 * Use this union in calls to
 *  - cmx_shm_set_value_single()
 *  - cmx_shm_get_value_single()
 */
union cmx_shm_value_t
{
    int64_t _int64;
    double _float64;
    int32_t _bool;
    cmx_shm_value_string _string;
};
typedef union cmx_shm_value_t cmx_shm_value;

// this is only a forward declaration, find the full definition in shm-private.h
struct cmx_shm_t;
typedef struct cmx_shm_t cmx_shm;

/**
 * \brief Unpack the value-index from the component handle value.
 *
 * A CMX Component reference is a 4 byte value. It is composed
 * of a value index and a value counter/id.
 *
 *  - If the sign is '-' (the number is negative) it is a error code.
 *
 *  - If the sign is '+' (the number is positive) it is a component handle
 *
 * \code
 *    |<- sign == -
 *    ,,<-                 error code   ->,       Error code
 *   --------------------------------------
 *   |X|24..30 | 16..23 |  8..15 |  0..7  |
 *   --------------------------------------
 *    ^^<-   counter  ->^<-    index    ->^       Component handle
 *    |<- sign == +
 * \endcode
 *
 * \return value_index
 */
static inline int cmx_shm_unpack_index(const int value_handle)
{
    return 0xffff & value_handle;
}

/**
 * \brief Unpack the value id
 *
 * This function does not check if the value_handle is valid or a error code.
 * \see unpack_index()
 * \return id
 */
static inline uint16_t cmx_shm_unpack_id(const int value_handle)
{
    return (uint16_t) (0x7fff & (value_handle >> 16));
}

/**
 * \brief Check if component handle is valid or errorcode.
 * \see unpack_index()
 * \return
 *      - ::CMX_TRUE (1) if value_handle < 0 / is a errorcode
 *      - ::CMX_FALSE (0) otherwise
 */
static inline int cmx_shm_unpack_iserror(const int value_handle)
{
    return (value_handle < 0) ? CMX_TRUE : CMX_FALSE;
}

/**
 * \brief Build a value_handle
 * \param id
 * \param index         caller have to make sure registry_index is in the range 0..2^16
 * \see unpack_index()
 * \return value_handle
 */
static inline int cmx_shm_pack_value_handle(const uint16_t id, const int index)
{
    return ((0x7fff & id) << 16) | (0xffff & index);
}

int cmx_shm_unmap(cmx_shm * const cmx_shm_ptr);

int cmx_shm_destroy(cmx_shm * const cmx_shm_ptr);

/**
 * \brief Create a new CMX Component.
 *
 * On success the argument pointer cmx_shm_ptr will set to the CMX Component.
 * Use cmx_shm_unmap() to free it. Use cmx_shm_destroy() to remove it.
 * \param name          Name of the Component with at least 1 and at most (CMX_NAME_SZ-1) characters.
 * \param cmx_shm_ptr
 * \return
 *      - ::E_CMX_SUCCESS
 *      - ::E_CMX_INVALID_ARGUMENT
 *      - ::E_CMX_CREATE_FAILED
 *      - ::E_NAME_EXISTS
 */
int cmx_shm_create(const char * const name, cmx_shm ** const cmx_shm_ptr);

/**
 * \brief Open a existing CMX Component
 *
 * On success the argument pointer cmx_shm_ptr will set to the CMX Component.
 * Use cmx_shm_unmap() to free it.
 * \param process_id
 * \param name          Name of the Component with at least 1 and at most (CMX_NAME_SZ-1) characters.
 * \param cmx_shm_ptr
 * \return
 *      - ::E_CMX_SUCCESS
 *      - ::E_CMX_NOT_FOUND
 *      - ::E_CMX_INVALID_ARGUMENT
 *      - ::E_CMX_CORRUPT_SEGMENT (if CMX_TAG mismatch)
 *      - ::E_CMX_OPERATION_FAILED (component found but failed to attach)
 */
int cmx_shm_open(const int process_id, const char * const name, cmx_shm ** const cmx_shm_ptr);

/**
 * \brief same as cmx_shm_open() but the shared memory is opened read-only.
 *
 * \see cmx_shm_open()
 */
int cmx_shm_open_ro(const int process_id, const char * const name, cmx_shm ** const cmx_shm_ptr);

/**
 *  \internal
 *  Not yet implemented.
 *  Will explicitly free slots marked as SLOT_STATE_FREE to save memory. Makes only sense
 *  after removing a huge amount of metrics and not replacing them with new ones.
 */
int cmx_shm_gc(cmx_shm * const cmx_shm_ptr);

/**
 * \brief Add a single-slot value (INT64,FLOAT64,BOOL)
 *
 * Currently there is \b no check for duplicate names.
 * \param cmx_shm_ptr
 * \param type               Value in cmx_value_type_t
 * \param name               Name of the new value
 * \returns
 *       - A CMX value-handle (> 0)
 *       - A CMX error code (< 0)
 *         - ::E_CMX_INVALID_ARGUMENT
 *         - ::E_CMX_INVALID_PID
 *         - ::E_CMX_COMPONENT_FULL
 */
int cmx_shm_add_value_single(cmx_shm * const cmx_shm_ptr, const int type, const char * const name);

/**
 * \brief Add a string value (CMX_TYPE_STRING)
 *
 * Currently there is \b no check for duplicate names.
 * \param cmx_shm_ptr
 * \param name            Name of the new value
 * \param size            no of characters (max. ::CMX_SHM_MAX_STRING_SZ)
 */
int cmx_shm_add_value_string(cmx_shm * const cmx_shm_ptr, const char * const name, const int size);

/**
 * \brief Find next value starting from a index position.
 *
 * Searchs slots in SET or UPDATE state and returns a value_handle and value_type. The search starts
 * at 'start' and returns on first match. The caller then can continue the search by extracting the index from
 * the value_handle and pass it +1 in argument start.
 * \param cmx_shm_ptr
 * \param start         Position where to start the search.
 * \param value_handle
 * \param value_type
 * \return
 *      - ::E_CMX_SUCCESS
 *      - or errorcode: TODO
 */
int cmx_shm_find_value(cmx_shm * const cmx_shm_ptr, int start, int * value_handle, int * value_type);

/**
 * \brief Set a single-slot value.
 * \note \ref REALTIME
 */
int cmx_shm_set_value_single(cmx_shm * const cmx_shm_ptr, const int value_handle, const int type,
        const cmx_shm_value * const value);

/**
 * \brief Set a string value
 *
 * This function copies exactly len bytes. Len must be <= size of the value slot.
 * \note \ref REALTIME
 */
int cmx_shm_set_value_string(cmx_shm * const cmx_shm_ptr, const int value_handle, const char * const value,
        const int len);

/**
 * \brief Read a single slot value
 *
 * For strings this function only returns size and next_index/id but not the actual value.
 *
 * This function first writes the data into the provided variables and after that decided if
 * the call is successful. That means even in case of error you end up with changed out-variables.
 *
 * \param cmx_shm_ptr
 * \param value_handle
 * \param[out] type
 * \param[out] value
 * \param[out] mtime         (may be null)
 * \return
 *      - ::E_CMX_SUCCESS
 *      - ::E_CMX_INVALID_ARGUMENT
 *      - ::E_CMX_INVALID_HANDLE  ('value_handle' is invalid)
 *      - ::E_CMX_CONCURRENT_MODIFICATION  (if a write happens during the read)
 * \note \ref REALTIME
 */
int cmx_shm_get_value_single(const cmx_shm * const cmx_shm_ptr, const int value_handle, int * const type,
        cmx_shm_value * const value, uint64_t * const mtime);

/**
 * \brief Type of callback function for cmx_shm_get_value_string()
 * \param buf       The data buffer.
 *                  Never try to manipulate this data. It points into cmx-managed shared-memory.
 *                  It is always terminated with a '\\0' but the '\\0' is not included in parameter 2.
 * \param len       Size of parameter 1 not including \\0
 * \param userarg   User-data, passed as 'arg' in cmx_shm_get_value_string.
 * \see cmx_shm_get_value_string()
 */
typedef void (*cmx_string_callback)(const char * buf, size_t len, void * userarg);

/**
 * \brief Read a string value
 * \param cmx_shm_ptr
 * \param value_handle
 * \param cb            Callback function. Is invoked for every chunk of data.
 * \param arg           pass user-data to cb
 * \param mtime (may be null)
 */
int cmx_shm_get_value_string(const cmx_shm * const cmx_shm_ptr, const int value_handle, cmx_string_callback cb,
        void * const arg, uint64_t * const mtime);

/**
 * \brief Read name of a value
 *
 * The name will be read into the user supplied buffer 'buf'. The size have to be at least CMX_NAME_SZ.
 * \return
 *      - ::E_CMX_INVALID_HANDLE The slot is either free or the handle has changed
 *          (more or less the same as a concurrent modification).
 *      - ::E_CMX_SUCCESS The name is successfully copied to 'buf'.
 */
int cmx_shm_get_value_name(const cmx_shm * const cmx_shm_ptr, const int value_handle, char * const buf);

/**
 * \brief Remove a value from a component
 *
 * A value referenced by 'value_handle' will be removed from a CMX Component. The type/name/.. is not checked.
 *
 * \param cmx_shm_ptr
 * \param value_handle
 * \return
 *      - ::E_CMX_SUCCESS
 *      - ::E_CMX_OPERATION_FAILED (the removal failed. The value-slot remains in OCCUPIED state, this is critical.)
 *      - ::E_CMX_INVALID_HANDLE
 *      - ::E_CMX_INVALID_PID
 *      - ::E_CMX_CONCURRENT_MODIFICATION
 *      - ::E_CMX_CORRUPT_SEGMENT
 */
int cmx_shm_remove_value(cmx_shm * const cmx_shm_ptr, const int value_handle);

#endif // HAVE_CMW_CMX_C_SHM_H
