/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/**
 * \internal
 * \file
 * \brief Shared Memory CMX Component, private API and structures
 */
#ifdef CMW_CMX_USE_PRIVATE
#undef CMW_CMX_USE_PRIVATE
#else
#warning "This Headerfile is private. If you really want to use it define CMW_CMX_USE_PRIVATE"
#endif
#ifndef HAVE_CMW_CMX_C_SHM_PRIVATE_H
#define HAVE_CMW_CMX_C_SHM_PRIVATE_H

#include <stddef.h>
#include <stdint.h>
#include <pthread.h>
#include <cmw-cmx/common.h>
#include <cmw-cmx/shm.h>

typedef struct cmx_shm_slot_value_t
{
    /// more-or-less unique id
    uint16_t id;
    /// value type indicator
    int32_t type;
    /// modify timestamp
    uint64_t mtime;
    /// name of this metric-value
    char name[CMX_NAME_SZ];
    /// transaction counter
    uint64_t txn;
    // bytes reserved for possible future usage
    char _reserved[30];
    cmx_shm_value value;
} cmx_shm_slot_value;

typedef struct cmx_shm_slot_payload_t
{
    /// more-or-less unique id
    uint16_t id;
    uint16_t next_index __attribute__ ((aligned (4)));
    uint16_t next_id __attribute__ ((aligned (4)));
    // the payload plus extra byte to place a null-termination
    char data[CMX_SHM_PAYLOAD_SZ + 1] __attribute__ ((aligned (4)));
} cmx_shm_slot_payload;

/// State of a CMX Value/Payload SLOT
enum cmx_shm_slot_state_t
{
    /// this slot is free (may be unwired)
    CMX_SLOT_STATE_FREE = 0,
    /// marked by gc to be freed, not yet used 2013-11-29
    CMX_SLOT_STATE_MARKED_GC = 1,
    /// The slot is allocated but not yet ready
    CMX_SLOT_STATE_OCCUPIED = 2,
    /// No update in process
    CMX_SLOT_STATE_SET = 3, // STATE_SET
    /// the updater is updating the value
    /// - All other updates are blocked
    /// - All reads are blocked
    CMX_SLOT_STATE_UPDATE = 4, // STATE_UPDATE
    /// The slot is a payload slot, for status see the corresponding value slot
    CMX_SLOT_STATE_PAYLOAD = 6
};

/**
 * \brief The CMX Shared Memory datastructure.
 */
struct cmx_shm_t
{
    /// \internal
    /// 8 chars, no null term.
    char CMX_TAG[CMX_SHM_TAG_LEN];
    int32_t process_id;
    char reserved[52];
    char name[CMX_NAME_SZ];
    int32_t value_state[CMX_NUMBER_OF_VALUES];
    union
    {
        cmx_shm_slot_value value;
        cmx_shm_slot_payload value_payload;
    } value[CMX_NUMBER_OF_VALUES] __attribute__ ((aligned (8)));
};

#define __compiletime_assert(condition, suffix, message) \
        { \
            extern void compiletime_assert_function_l ## suffix(void) __attribute__((error(message))); \
            if (!(condition)) \
                compiletime_assert_function_l ## suffix(); \
        }
#define _compiletime_assert(condition, __LINE__, message) __compiletime_assert(condition, __LINE__, message)
#define c_static_assert(condition, message) _compiletime_assert(condition, __LINE__, message)
#define static_assert_eq(expected,actual,message) \
    c_static_assert(expected==actual, \
            "\n[ TEST FAILED ] " message \
            "\nValue of: " #actual \
            "\nExpected: " #expected)

static void check__cmx_shm_value_string() __attribute((unused));
static void check__cmx_shm_value_string()
{
    static_assert_eq(0U, offsetof(cmx_shm_value_string, size), "Check Offset");
    static_assert_eq(2U, offsetof(cmx_shm_value_string, current_size), "Check Offset");
    static_assert_eq(4U, offsetof(cmx_shm_value_string, next_index), "Check Offset");
    static_assert_eq(6U, offsetof(cmx_shm_value_string, next_id), "Check Offset");
    static_assert_eq(8U, sizeof(cmx_shm_value_string), "Size of cmx_shm_value_string");
}

static void check__cmx_shm_value() __attribute((unused));
static void check__cmx_shm_value()
{
    static_assert_eq(0U, offsetof(cmx_shm_value, _int64), "Check Offset");
    static_assert_eq(0U, offsetof(cmx_shm_value, _float64), "Check Offset");
    static_assert_eq(0U, offsetof(cmx_shm_value, _bool), "Check Offset");
    static_assert_eq(0U, offsetof(cmx_shm_value, _string), "Check Offset");
    static_assert_eq(8U, sizeof(cmx_shm_value), "size of cmx_shm_value");
}

static void check__cmx_shm_slot_value() __attribute((unused));
static void check__cmx_shm_slot_value()
{
    static_assert_eq(0U, offsetof(cmx_shm_slot_value, id), "Check Offset");
    static_assert_eq(4U, offsetof(cmx_shm_slot_value, type), "Check Offset");
    static_assert_eq(8U, offsetof(cmx_shm_slot_value, mtime), "Check Offset");
    static_assert_eq(16U, offsetof(cmx_shm_slot_value, name), "Check Offset");
    static_assert_eq(80U, offsetof(cmx_shm_slot_value, txn), "Check Offset");
    static_assert_eq(88U, offsetof(cmx_shm_slot_value, _reserved), "Check Offset");
    static_assert_eq(120U, offsetof(cmx_shm_slot_value, value), "Check Offset");
    static_assert_eq(128U, sizeof(cmx_shm_slot_value), "size of cmx_shm_slot_value");
}

static void check__cmx_shm_slot_payload() __attribute((unused));
static void check__cmx_shm_slot_payload()
{
    static_assert_eq(0U, offsetof(cmx_shm_slot_payload, id), "Check Offset");
    static_assert_eq(4U, offsetof(cmx_shm_slot_payload, next_index), "Check Offset");
    static_assert_eq(8U, offsetof(cmx_shm_slot_payload, next_id), "Check Offset");
    static_assert_eq(12U, offsetof(cmx_shm_slot_payload, data), "Check Offset");

    static_assert_eq(12U + CMX_SHM_PAYLOAD_SZ + 1U, sizeof(cmx_shm_slot_payload), "Check size");
    static_assert_eq(12U + 115U + 1U, sizeof(cmx_shm_slot_payload), "Check size");
}

static void check__cmx_shm() __attribute((unused));
static void check__cmx_shm()
{
    static_assert_eq(0U, offsetof(cmx_shm, CMX_TAG), "Check Offset");
    static_assert_eq(0U + 8U, offsetof(cmx_shm, process_id), "Check Offset");
    static_assert_eq(12U, offsetof(cmx_shm, reserved), "Check Offset");
    static_assert_eq(64U, offsetof(cmx_shm, name), "Check Offset");
    static_assert_eq(128U, offsetof(cmx_shm, value_state), "Check Offset");
    static_assert_eq(128U + 81920U, offsetof(cmx_shm, value), "Check Offset");
    static_assert_eq(128U + 81920U + 2621312U, sizeof(cmx_shm), "Check size");
}

static void check__cmx_shm_alignment() __attribute((unused));
static void check__cmx_shm_alignment()
{

    // check alignment
    static_assert_eq(8U, sizeof((((cmx_shm* )0)->value[0].value.txn)), "Size of cmx_shm_slot_value.TXN");
    static_assert_eq(0U, offsetof(cmx_shm, value[1].value.txn) % 8, "Alignment of txn");
	static_assert_eq(0U, offsetof(cmx_shm, value_state[0]) % 4, "Alignment of value_state fields");
	static_assert_eq(0U, offsetof(cmx_shm, value_state[1]) % 4, "Alignment of value_state fields");
}

#endif // HAVE_CMW_CMX_C_SHM_PRIVATE_H
