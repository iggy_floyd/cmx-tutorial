/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/** \file
 *  \brief CMX logging functions
 */
#ifndef HAVE_CMW_CMX_C_LOG_H
#define HAVE_CMW_CMX_C_LOG_H

#include <stdio.h>
#include <stdarg.h>

/** @name Logging macros
 * Logging Compile-time configuration:
 * - LOG_NOTHING    : Disables all logging
 * - LOG_ONLY_AUDIT : compile AUDIT log statements
 * - LOG_ONLY_ERROR : compile AUDIT, ERROR log statements
 * - LOG_ONLY_WARN  : compile AUDIT, ERROR, WARN log statements
 * - LOG_ONLY_INFO  : compile AUDIT, ERROR, WARN, INFO log statements
 * - LOG_ONLY_DEBUG : compile AUDIR, ERROR, WARN, INFO, DEBUG log statements
 * - no define      : compile all log statements
 */
///@{
/**
 * \brief Enumeration of all valid logging levels
 */
enum cmx_log_levels
{
    LOG_LEVEL_AUDIT = 0, ///< highest logging level
    LOG_LEVEL_ERROR = 1, ///< \see log_levels
    LOG_LEVEL_WARN = 2, ///<  \see log_levels
    LOG_LEVEL_INFO = 3, ///<  \see log_levels
    LOG_LEVEL_DEBUG = 4, ///< \see log_levels
    LOG_LEVEL_TRACE = 5 ///<  lowest, most detailed logging level
};

#ifndef __cplusplus
/// internal log macro
#define _LOG_LOG(level, ...) \
        cmx_log_printf(level, __FILE__, __LINE__, __FUNCTION__,  __VA_ARGS__)
#endif // defined __cplusplus

/**
 * \brief Type of a log adapter function.
 *
 * The internal stdout-logger can be replaced with a function
 * of this type.
 *
 * -# enum log_levels: The log level (::log_levels).
 * -# const char *: The source file
 * -# int : line number
 * -# const char *: the source function name
 * -# const char *: the message format
 * -# va_list:, the argument for the previous format string
 */
typedef void (*cmx_log_adapter_t)(enum cmx_log_levels, const char *, int, const char *, const char *, va_list);

/**
 * \brief Set a external logging function.
 *
 * The type of the function is defined as cmx_log_adapter_t.
 */
void cmx_log_set_adapter(cmx_log_adapter_t log_adapter_func);

/**

 */

#ifndef __cplusplus
/// Audit Log entry
#if defined(LOG_NOTHING)
#define LOG_AUDIT_IF(...)
#else
#define LOG_AUDIT_IF(...)  _LOG_LOG(LOG_LEVEL_AUDIT, __VA_ARGS__)
#endif

/// Error Log entry
#if defined(LOG_NOTHING) || defined(LOG_ONLY_AUDIT)
#define LOG_ERROR_IF(...)
#else
#define LOG_ERROR_IF(...)  _LOG_LOG(LOG_LEVEL_ERROR, __VA_ARGS__)
#endif

/// Warn Log entry
#if defined(LOG_NOTHING) || defined(LOG_ONLY_AUDIT) || defined(LOG_ONLY_ERROR)
#define LOG_WARN_IF(...)
#else
#define LOG_WARN_IF(...)   _LOG_LOG(LOG_LEVEL_WARN, __VA_ARGS__)
#endif

/// Info Log entry
#if defined(LOG_NOTHING)    || defined(LOG_ONLY_AUDIT) || \
    defined(LOG_ONLY_ERROR) || defined(LOG_ONLY_WARN)
#define LOG_INFO_IF(...)
#else
#define LOG_INFO_IF(...)   _LOG_LOG(LOG_LEVEL_INFO, __VA_ARGS__)
#endif

/// Debug Log entry
#if defined(LOG_NOTHING)    || defined(LOG_ONLY_AUDIT) || \
    defined(LOG_ONLY_ERROR) || defined(LOG_ONLY_WARN)  || \
    defined(LOG_ONLY_INFO)
#define LOG_DEBUG_IF(...)
#else
#define LOG_DEBUG_IF(...)  _LOG_LOG(LOG_LEVEL_DEBUG, __VA_ARGS__)
#endif

/// Trace Log entry
#if defined(LOG_NOTHING)    || defined(LOG_ONLY_AUDIT) || \
    defined(LOG_ONLY_ERROR) || defined(LOG_ONLY_WARN)  || \
    defined(LOG_ONLY_INFO)  || defined(LOG_ONLY_DEBUG)
#define LOG_TRACE_IF(...)
#else
#define LOG_TRACE_IF(...)  _LOG_LOG(LOG_LEVEL_TRACE, __VA_ARGS__)
#endif
#endif // defined __cplusplus

///@}

/** @name Logging control functions
 */
///@{
/**
 * \brief Change log level for a code-block.
 *
 * This macro changes the logging level for the following code-block and
 * restores the previous logging level afterwards.
 *
 * The newly introduced local variables _loop and _old_log_level
 * must remain unmodified by the caller.
 */
#define LOG_SET_LEVEL(l) \
    for (int _loop=1, _old_log_level = cmx_log_current_level; \
         _loop == 1 && ((cmx_log_current_level = l) || 1) ; \
         cmx_log_current_level = _old_log_level, _loop=0)

/**
 * \brief Read the log_level from environment variable LOG_LEVEL
 * \see cmx_log_from_string()
 */
enum cmx_log_levels cmx_log_level_from_env(void);

/**
 * \brief Parse a string to log-level.
 *
 * On parse error: return default log-level INFO.
 */
enum cmx_log_levels cmx_log_from_string(const char *level);

/**
 * \brief the current logging level.
 *
 * This variable can but should usually not be manipulated directly.
 * The valid values are defined in ::log_levels.
 */
extern int cmx_log_current_level;

/**
 * \brief convert a log level to string like "INFO".
 *
 * A unknown log level is returned as empty string.
 */
const char *cmx_log_level_name(enum cmx_log_levels level);

/**
 * \brief Logging implementation
 *
 * If a logadapter is set all logging is dispatched to the log_adapter function.
 * If not a logging message is printed to stdout if cmx_log_current_level >= 'level'.
 */
void cmx_log_vprintf(enum cmx_log_levels level, const char * file, const int line, const char * function, const char * fmt,
        va_list ap);

/**
 * \brief redirects to cmx_log_vprintf()
 */
void cmx_log_printf(enum cmx_log_levels level, const char * file, const int line, const char * function, const char * fmt,
        ...) __attribute__ ((format (printf, 5, 6)));
///@}

#endif // HAVE_CMW_CMX_C_LOG_H
