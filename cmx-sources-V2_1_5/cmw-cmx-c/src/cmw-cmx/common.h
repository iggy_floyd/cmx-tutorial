/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/** \file
 *  \brief Common CMX functions
 */
#ifndef HAVE_CMW_CMX_C_COMMON_H
#define HAVE_CMW_CMX_C_COMMON_H

#include <stdint.h>

/// Boolean false value
#define CMX_FALSE                          0
/// Boolean true value
#define CMX_TRUE                           1
/// \brief The CMX tag is written in the first 8 bytes of a shared memory objects.
#define CMX_SHM_TAG "CMX_1  \0"
/// \brief Length of the CMX Version Tag.
#define CMX_SHM_TAG_LEN 8
/// \brief Size of identifiers
#define CMX_NAME_SZ 64
/// \brief Number of bytes in a single payload slot.
#define CMX_SHM_PAYLOAD_SZ 115
/// \brief CMX_SHM_PAYLOAD_SZ
/// A string single is maximum 560 slots long.
/// Thats means maximum 64,4kB: 115 Bytes/Slot * 560 Slots = 64400 Bytes
/// This value is limited by the uint16_t size counter in cmx_shm_value_string.
#define CMX_SHM_MAX_STRING_SZ 64400
/// \brief Number of values
#define CMX_NUMBER_OF_VALUES 0x4fff
/// \brief Magic end-marking index value for strings
#define CMX_STRING_END_INDEX 0xffff

/// CMX error codes. Use cmx_common_strerror() for to-string conversion.
enum cmx_cmx_error
{
    E_CMX_SUCCESS = 0, ///< \see cmx_cmx_error
    E_CMX_OPERATION_FAILED = -1, ///< \see cmx_cmx_error
    E_CMX_INVALID_HANDLE = -2, ///< \see cmx_cmx_error
    E_CMX_TYPE_MISMATCH = -3, ///< \see cmx_cmx_error
    E_CMX_CORRUPT_SEGMENT = -4, ///< \see cmx_cmx_error
    E_CMX_INVALID_ARGUMENT = -5, ///< \see cmx_cmx_error
    E_CMX_OUT_OF_MEMORY = -6, ///< \see cmx_cmx_error
    E_CMX_CREATE_FAILED = -7, ///< \see cmx_cmx_error
    E_CMX_INVALID_PID = -8, ///< \see cmx_cmx_error
    E_CMX_CONCURRENT_MODIFICATION = -9, ///< \see cmx_cmx_error
    E_CMX_NAME_EXISTS = -10, ///< \see cmx_cmx_error
    E_CMX_NOT_FOUND = -11, ///< \see cmx_cmx_error
    E_CMX_COMPONENT_FULL = -12, ///< \see cmx_cmx_error
    E_CMX_PROTOCOL_VERSION = -13, ///< \see cmx_cmx_error
    E_CMX_OUT_OF_RANGE = -14, ///< \see cmx_cmx_error
    E_CMX_UNUSED_3 = -15, ///< \see cmx_cmx_error
    E_CMX_UNUSED_1 = -16, ///< \see cmx_cmx_error
    E_CMX_UNUSED_2 = -17 ///< \see cmx_cmx_error
};

/// returns a string description of a cmx_cmx_error code
const char * cmx_common_strerror(const int code);

/**
 * \brief Check length of a character string.
 *
 * Returns ::CMX_FALSE
 * - if pointer `string` is NULL.
 * - if string has less than \e min_size characters
 * - if string has max_size characters or more.
 * \return
 *          - ::CMX_TRUE if string is null-terminated and in given size range.
 *          - ::CMX_FALSE otherwise.
 */
int cmx_common_check_string_length(const char * string, unsigned int min_size, unsigned int max_size);

/**
 * \brief Get current time in usec.
 *
 * \return
 *  - The current time, timestamp in usec since begin of epoch.
 *  - 0 on error.
 */
uint64_t cmx_common_current_time_usec(void);

/**
 * \return the PID of the current process as reported by the system.
 */
int cmx_common_current_pid(void);

#endif // HAVE_CMW_CMX_C_COMMON_H
