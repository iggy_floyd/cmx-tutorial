#ifndef HAVE_CMW_CMX_C_PROCESS_H
#define HAVE_CMW_CMX_C_PROCESS_H
/** \file
 *  \brief Process Component functions
 */

/**
 * \brief initialize or update the process information in cmx.
 *
 * If called in the current process for the first time, this function will create a so-called
 * CMX Process-Component.
 *
 * Then and at any consequent call, the values in the process component will get updated.
 *
 * Currently the process component contains the following metrics:
 *
 * process_name - program_invocation_short_name - value initialized once.
 * process_id - getpid() - value initialized once.
 * hostname - gethostname() - value initialized once.
 * start_time - time() - value initialized once.
 * rusage.{utime,stime,nvcsw,nivcsw} - getrusage() - updated at every call.
 *
 * In the future more predefined metrics will be added into the process component.
 *
 * The calling thread must be aware that this function executes syscalls.
 * Do not call it too frequently and never from real-time threads.
 */
void cmx_process_update(void);

#endif // HAVE_CMW_CMX_C_PROCESS_H
