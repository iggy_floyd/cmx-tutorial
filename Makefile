# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project




all: configuration doc README.wiki build test configure 


# to check that the system has all needed components
configuration: configure
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac



doc: README.wiki
	-@echo "\n\nDoc building...\n\n"
	-@ mkdir doc
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc; rm -r {}" | sh


test: configuration build
	-@echo "\n\nTesting...\n\n"
	-@ echo "[test]: Not implemented"



build:
	-@echo "Building...\n\n"
	-@for dir in `find cmx-sources-V2_1_5/  -maxdepth 1 -type d | sort`; do echo $(dir); cd $$dir;  $(scons)  ; cd - 2> /dev/null; done 
	 


run: configuration build
	-@echo "\n\nRunning...\n\n"
	-@ echo "[run]: Not implemented"


# to clean all temporary stuff
scons:=$(shell echo `pwd`)/scons-build/bin/scons
clean: 
	-@echo "\n\nCleaning...\n\n"
	-@rm -r config.log autom4te.cache
	-@for dir in `find cmx-sources-V2_1_5/  -maxdepth 1 -type d`; do echo $(dir); cd $$dir;  $(scons) -c ; cd - 2> /dev/null; done



.PHONY: configuration clean all doc run test build
