== Abstract ==

''' @2014 I. Marfin '''   ''<Unister Gmbh>''


This is a tutorial on application of a CMX system and the Scons tool.




==  Brief HOW-TO  ==


* To start using the package in your project, you have to prepare it  following the steps:



         1) check that you have installed components needed

                source ./make 

         
* To clean the project ( the '''cleaning''' can be used in Automake or Makefile ), please do

              make clean

* To test the project

              make test



== Contact ==

Igor Marfin ''<Igor.Marfin@unister-gmbh.de>''

